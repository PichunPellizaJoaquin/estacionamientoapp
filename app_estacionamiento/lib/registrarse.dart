import 'package:flutter/material.dart';
import 'iniciarsesion.dart';
import 'main.dart';

class Registrarse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text(
          "Registrarse",
          style: TextStyle(
              fontSize: 20.0,
              decoration: TextDecoration.underline,
              fontFamily: 'space age'),
        ),
      ),
      body: FormularioRegistrarse(),
    );
  }
}

class FormularioRegistrarse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Registrarse',
      home: Scaffold(
        body: RegistrarseScreen(),
      ),
    );
  }
}

class RegistrarseScreen extends StatefulWidget {
  @override
  _RegistrarseScreenState createState() => new _RegistrarseScreenState();
}

class _RegistrarseScreenState extends State<RegistrarseScreen> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.all(20),
      child: Form(
          child: Column(
        children: <Widget>[
          nombreapellidoField(),
          dniField(),
          nacimientoField(),
          patenteField(),
          marcaField(),
          modeloField(),
          claveField(),
          claveconfirmacionField(),
          buttonField(),
        ],
      )),
    );
  }

  Widget nombreapellidoField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Nombre/s y Apellido/s',
          hintText: 'Nombre/s y Apellido/s'),
    );
  }

  Widget dniField() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'D.N.I', hintText: 'D.N.I'),
    );
  }

  Widget nacimientoField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Fecha de nacimiento', hintText: 'Fecha de nacimiento'),
    );
  }

  Widget patenteField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Patente Vehiculo', hintText: 'Patente Vehiculo'),
    );
  }

  Widget marcaField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Marca Vehiculo', hintText: 'Marca Vehiculo'),
    );
  }

  Widget modeloField() {
    return TextFormField(
      decoration: InputDecoration(
          labelText: 'Modelo Vehiculo', hintText: 'Modelo Vehiculo'),
    );
  }

  Widget claveField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Contraseña',
        hintText: 'Contraseña',
      ),
    );
  }

  Widget claveconfirmacionField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Confirmar Contraseña',
        hintText: 'Confirmar Contraseña',
      ),
    );
  }

  Widget buttonField() {
    return Center(
      child: Container(
        child: Row(
          children: [
            SizedBox(width: 115),
            SizedBox(
              width: 100.0,
              height: 50.0,
              child: RaisedButton(
                color: Colors.lightGreen[400],
                child: Text(
                  'registrar',
                  style: TextStyle(
                      fontSize: 18, color: Colors.white, fontFamily: 'segoeui'),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => IniciarSesion(),
                  ));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

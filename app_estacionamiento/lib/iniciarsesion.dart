import 'principal.dart';
import 'main.dart';
import 'package:flutter/material.dart';

class IniciarSesion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text(
          "Iniciar Sesion",
          style: TextStyle(
              fontSize: 20.0,
              decoration: TextDecoration.underline,
              fontFamily: 'space age'),
        ),
      ),
      body: FormularioIniciarSesion(),
    );
  }
}

class FormularioIniciarSesion extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Iniciar Sesion',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }
}

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => new _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.all(20),
      child: Form(
          child: Column(
        children: <Widget>[
          dniField(),
          claveField(),
          buttonField(),
        ],
      )),
    );
  }

  Widget dniField() {
    return TextFormField(
      decoration: InputDecoration(labelText: 'D.N.I', hintText: 'D.N.I'),
    );
  }

  Widget claveField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Contraseña',
        hintText: 'Contraseña',
      ),
    );
  }

  Widget buttonField() {
    return Center(
      child: Container(
        child: Row(
          children: [
            SizedBox(width: 115),
            SizedBox(
              width: 100.0,
              height: 50.0,
              child: RaisedButton(
                color: Colors.lightGreen[400],
                child: Text(
                  'iniciar',
                  style: TextStyle(
                      fontSize: 18, color: Colors.white, fontFamily: 'segoeui'),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Principal(),
                  ));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'iniciarsesion.dart';
import 'registrarse.dart';
import 'principal.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
      home: MyApp(), theme: ThemeData(primaryColor: Colors.white)));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        title: Text(
          "Estacionamiento Libre",
          style: TextStyle(
              fontSize: 20.0,
              decoration: TextDecoration.underline,
              fontFamily: 'space age'),
        ),
      ),
      body: Botones(),
    );
  }
}

class Botones extends StatelessWidget {
  const Botones({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 30),
            SizedBox(
              child: Container(
                child: Image.asset('assets/images/logo.png'),
              ),
            ),
            SizedBox(height: 50),
            SizedBox(
              width: 250.0,
              height: 50.0,
              child: RaisedButton(
                color: Colors.lightGreen[400],
                shape: StadiumBorder(),
                child: Text(
                  'INICIAR SESION',
                  style: TextStyle(
                      fontSize: 29, color: Colors.white, fontFamily: 'segoeui'),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => IniciarSesion(),
                  ));
                },
              ),
            ),
            SizedBox(height: 30),
            SizedBox(
              width: 250.0,
              height: 50.0,
              child: RaisedButton(
                color: Colors.lightGreen[400],
                shape: StadiumBorder(),
                child: Text(
                  'REGISTARSE',
                  style: TextStyle(
                      fontSize: 29, color: Colors.white, fontFamily: 'segoeui'),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Registrarse(),
                  ));
                },
              ),
            ),
            SizedBox(height: 30),
            SizedBox(
              width: 250.0,
              height: 50.0,
              child: OutlineButton(
                color: Colors.lightGreen[400],
                shape: StadiumBorder(),
                child: Text(
                  'Google',
                  style: TextStyle(
                      fontSize: 29, color: Colors.black, fontFamily: 'segoeui'),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => Principal(),
                  ));
                },
              ),
            ),
          ]),
    ));
  }
}
